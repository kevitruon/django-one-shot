from django.urls import path
from todos.views import todo_list, show_todo, create_todo, delete_todo, edit_todo, edit_todo_item, create_todo_item

urlpatterns = [
    path("todos/", todo_list, name="todo_list_list"),
    path("todos/<int:id>/", show_todo, name = "todo_list_detail"),
    path("todos/create/", create_todo, name = "todo_list_create"),
    path("todos/<int:id>/delete/", delete_todo, name = "todo_list_delete"),
    path("todos/<int:id>/edit/", edit_todo, name = "todo_list_update"),
    path("todos/items/create/", create_todo_item, name = "todo_item_create"),
    path("todos/items/<int:id>/edit/", edit_todo_item, name = "todo_item_update"),

]
