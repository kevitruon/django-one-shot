from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoForm, TodoItemForm

# Create your views here.
def todo_list(request):
    todos = TodoList.objects.all()
    context = {
        "todo_list": todos,
    }
    return render(request, "todos/list.html", context)
def redirect_to_todo_list(request):
    return redirect("todo_list_list")

def show_todo(request,id):
    todo = get_object_or_404(TodoList, id = id)
    context = {
        "todo_object":todo,
        "id": id,
    }
    return render(request, "todos/detail.html", context)

def create_todo(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            list = form.save()
            list.save()
            return redirect("todo_list_detail", list.id)
    else:
        form = TodoForm()
    context = {
        "form": form,
    }
    return render(request, "todos/create.html", context)

def delete_todo(request,id):
    todo = get_object_or_404(TodoList, id = id)
    if request.method == "POST":
        todo.delete()
        return redirect("todo_list_list")
    context = {
        "todo_list": todo,
    }
    return render(request, "todos/delete.html", context)

def edit_todo(request, id):
    todo = get_object_or_404(TodoList, id = id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", todo.id)
    else:
        form = TodoForm(instance=todo)
        context = {
            "todo_object": todo,
            "form": form,
        }
        return render(request, "todos/edit.html", context)


def create_todo_item(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            todo_item = form.save()
            return redirect("todo_list_detail", id=todo_item.list.id)
    else:
        form = TodoItemForm()

    context = {
        "form": form,
    }
    return render(request, "todos/createItem.html", context)

def edit_todo_item(request, id):
    item = get_object_or_404(TodoItem, id=id)
    if request.method == "POST":
        form = TodoItemForm(request.POST, instance=item)
        if form.is_valid():
            form.save()
            return redirect("todo_list_detail", id= item.id)
    else:
        form = TodoItemForm(instance=item)

    context = {
        "form": form,
        "todo_list_id": item.list.id,
    }

    return render(request, "todos/editItem.html", context)
